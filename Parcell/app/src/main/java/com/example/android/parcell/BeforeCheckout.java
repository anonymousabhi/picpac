package com.example.android.parcell;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BeforeCheckout extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;
    ThirdAdapter thirdAdapter;
    Intent intent;
    TextView textView;
    List<Searchitem> data = new ArrayList<>();
    Spinner pickup_spinner, destination_spinner;
    com.beardedhen.androidbootstrap.BootstrapButton button_next;
    TextView to, from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_before);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setup(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        textView = (TextView) findViewById(R.id.textview_additems);
        recyclerView = (RecyclerView) findViewById(R.id.searchList);
        thirdAdapter = new ThirdAdapter(this, set());
        recyclerView.setAdapter(thirdAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        pickup_spinner = (Spinner) findViewById(R.id.pick_up_spinner);
        destination_spinner = (Spinner) findViewById(R.id.destination_spinner);
        ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(this, R.array.pick_up, R.layout.support_simple_spinner_dropdown_item);
        pickup_spinner.setAdapter(arrayAdapter);
        pickup_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                from = (TextView) view;
                if (from.getText().equals("Delhi")) {
                    ArrayAdapter adapter = ArrayAdapter.createFromResource(BeforeCheckout.this, R.array.destination, R.layout.support_simple_spinner_dropdown_item);
                    destination_spinner.setAdapter(adapter);
                }
                if (from.getText().equals("Noida")) {
                    ArrayAdapter adapter = ArrayAdapter.createFromResource(BeforeCheckout.this, R.array.noidadestination, R.layout.support_simple_spinner_dropdown_item);
                    destination_spinner.setAdapter(adapter);
                }
                if (from.getText().equals("Gurgaon")) {
                    ArrayAdapter adapter = ArrayAdapter.createFromResource(BeforeCheckout.this, R.array.gurgaondestination, R.layout.support_simple_spinner_dropdown_item);
                    destination_spinner.setAdapter(adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        destination_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                to = (TextView) view;


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        button_next = (BootstrapButton) findViewById(R.id.next);
        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BeforeCheckout.this, checkout.class);
                intent.putExtra("from", from.getText());
                intent.putExtra("to", to.getText());
                startActivity(intent);

            }
        });
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences("ListData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                String arr;
                for (int i = 0; i < data.size(); i++) {
                    Searchitem searchitem;
                    searchitem = data.get(i);
                    arr = searchitem.itemname;
                    editor.putString("list" + Integer.toString(i), arr);
                    editor.apply();
                }
                intent = new Intent(BeforeCheckout.this, List_details.class);
                startActivity(intent);
            }
        });
    }

    public List<Searchitem> set() {
        intent = getIntent();
        int flag = 0;
        Searchitem cur = new Searchitem();
        cur.itemname = intent.getStringExtra("Itemname");
        SharedPreferences sharedPreferences = getSharedPreferences("ListData", Context.MODE_PRIVATE);
        Map<String, ?> entries = sharedPreferences.getAll();
        for (int i = 0; i < entries.size(); i++) {
            Searchitem current = new Searchitem();
            current.itemname = sharedPreferences.getString("list" + Integer.toString(i), null);
            if (!cur.itemname.equals(current.itemname)) {
                data.add(current);
            }
            if (cur.itemname.equals(current.itemname)) {
                flag = 1;
                data.add(cur);
            }
        }
        if (entries.size() == 0 || flag == 0) {
            data.add(cur);
        }
        return data;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences sharedPreferences = getSharedPreferences("ListData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        startActivity(new Intent(this, MainActivity.class));
    }

}
