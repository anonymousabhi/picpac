package com.example.android.parcell;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beardedhen.androidbootstrap.AwesomeTextView;
import com.beardedhen.androidbootstrap.BootstrapButton;
import com.example.android.parcell.beckend.ConnectionDetector;
import com.example.android.parcell.beckend.CustomRequest;
import com.example.android.parcell.beckend.ErrorDialogMessage;
import com.example.android.parcell.beckend.Progress;
import com.example.android.parcell.beckend.VolleySingleton;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class Login extends AppCompatActivity implements View.OnClickListener {
    static Boolean userLoggedin = false;
    Toolbar toolbar;
    RequestQueue queue;
    com.rengwuxian.materialedittext.MaterialEditText editText_email, editText_password;
    CheckBox checkBox;
    com.beardedhen.androidbootstrap.BootstrapButton button_login, button_singnup;
    com.beardedhen.androidbootstrap.AwesomeTextView textView;
    android.support.design.widget.TextInputLayout Erroremail, Errorpassword;
    Progress progress = new Progress(this);
    ConnectionDetector connectionDetector = new ConnectionDetector(this);
    ErrorDialogMessage errorDialogMessage = new ErrorDialogMessage(this);

    static boolean isUserLoggedin() {
        return userLoggedin;
    }

    static void setUserLoggedin(Boolean user) {
        userLoggedin = user;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    String s;
    Intent intent = null;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        intent = getIntent();
        s = intent.getStringExtra("intent");
        queue = VolleySingleton.getInstance().getRequestQueue();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        editText_email = (MaterialEditText) findViewById(R.id.et_email);
        editText_password = (MaterialEditText) findViewById(R.id.et_password);
        button_login = (BootstrapButton) findViewById(R.id.button_login);
        button_singnup = (BootstrapButton) findViewById(R.id.button_signup);
        textView = (AwesomeTextView) findViewById(R.id.text_forget);
        Erroremail = (TextInputLayout) editText_email.getParent();
        Errorpassword = (TextInputLayout) editText_password.getParent();
        checkBox = (CheckBox) findViewById(R.id.checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    editText_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    editText_password.setInputType(129);
                }

            }
        });
        textView.setOnClickListener(this);
        button_login.setOnClickListener(this);
        button_singnup.setOnClickListener(this);
    }

    private void generateError(TextInputLayout t, String Message) {
        t.setErrorEnabled(true);
        t.setError(Message);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void parseData(String email, String password) {


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("password", password);
        String url = "http://dexesnotes.net84.net/login.php";
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String data = response.getString("login");
                    if (data.equals("data found")) {
                        userLoggedin = true;
                        save();
                        Login.setUserLoggedin(userLoggedin);
                        errorDialogMessage.show("LOGIN SUCCESSFUL!!");
                        if (s.equals("one")) {
                            startActivity(new Intent(Login.this, GetAddress.class));
                        } else if (s.equals("two")) {
                            onBackPressed();
                        }
                    } else {
                        errorDialogMessage.show("YOU ARE NOT REGISTERED");
                    }
                    progress.stop();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.stop();
            }
        });
        jsObjRequest.setTag("Login");
        queue.add(jsObjRequest);
    }

    private void save() {
        Boolean b = Login.isUserLoggedin();
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = openFileOutput("Login.txt", Context.MODE_PRIVATE);
            if (b == true) {
                fileOutputStream.write(1);
            } else {
                fileOutputStream.write(0);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_login:
                if (connectionDetector.isConnectingToInternet()) {
                    int count = 0;
                    String email, password;
                    email = editText_email.getText().toString();
                    password = editText_password.getText().toString();
                    if (password.length() < 8) {
                        generateError(Errorpassword, "          *Min 8 Characters");
                    } else {
                        count++;
                        Errorpassword.setErrorEnabled(false);
                    }

                    if (!isValidEmail(email)) {
                        generateError(Erroremail, "          *Enter Valid Email");
                    } else {
                        count++;
                        Erroremail.setErrorEnabled(false);
                    }
                    if (count == 2) {
                        progress.show();
                        parseData(email, password);
                    }
                } else {
                    errorDialogMessage.show();
                }
                break;
            case R.id.button_signup:
                intent = new Intent(this, Signup.class);
                intent.putExtra("intent", s);
                startActivity(intent);
                break;
            case R.id.text_forget:
                intent = new Intent(new Intent(this, Forget.class));
                startActivity(intent);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (s.equals("two")) {
            startActivity(new Intent(this, MainActivity.class));
        } else if (s.equals("one")) {
            startActivity(new Intent(Login.this, BeforeCheckout.class));
        }
    }
}