package com.example.android.parcell;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.beardedhen.androidbootstrap.BootstrapEditText;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    com.beardedhen.androidbootstrap.BootstrapEditText editTextsearch;
    ImageView image1, image2, image3, image4, image5, image6, image7, image8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setup(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        editTextsearch = (BootstrapEditText) findViewById(R.id.et_search);
        editTextsearch.setOnClickListener(this);
        editTextsearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                startActivity(new Intent(MainActivity.this, List_details.class));
            }
        });
        image1 = (ImageView) findViewById(R.id.clothes);
        image2 = (ImageView) findViewById(R.id.gifts);
        image3 = (ImageView) findViewById(R.id.laptop);
        image4 = (ImageView) findViewById(R.id.mobile);
        image5 = (ImageView) findViewById(R.id.watch);
        image6 = (ImageView) findViewById(R.id.camera);
        image7 = (ImageView) findViewById(R.id.books);
        image8 = (ImageView) findViewById(R.id.documents);
        image1.setOnClickListener(this);
        image2.setOnClickListener(this);
        image3.setOnClickListener(this);
        image4.setOnClickListener(this);
        image5.setOnClickListener(this);
        image6.setOnClickListener(this);
        image7.setOnClickListener(this);
        image8.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        Intent intent;
        String s;
        switch (view.getId()) {
            case R.id.et_search:
                startActivity(new Intent(this, List_details.class));
                break;

            case R.id.clothes:
                s = "Clothes";
                intent = new Intent(this, BeforeCheckout.class);
                intent.putExtra("Itemname", s);
                startActivity(intent);
                break;
            case R.id.gifts:
                s = "Gifts";
                intent = new Intent(this, BeforeCheckout.class);
                intent.putExtra("Itemname", s);
                startActivity(intent);
                break;
            case R.id.laptop:
                s = "Laptop";
                intent = new Intent(this, BeforeCheckout.class);
                intent.putExtra("Itemname", s);
                startActivity(intent);
                break;
            case R.id.mobile:
                s = "Mobile";
                intent = new Intent(this, BeforeCheckout.class);
                intent.putExtra("Itemname", s);
                startActivity(intent);
                break;
            case R.id.watch:
                s = "Watch";
                intent = new Intent(this, BeforeCheckout.class);
                intent.putExtra("Itemname", s);
                startActivity(intent);
                break;
            case R.id.camera:
                s = "Camera";
                intent = new Intent(this, BeforeCheckout.class);
                intent.putExtra("Itemname", s);
                startActivity(intent);
                break;
            case R.id.books:
                s = "Books";
                intent = new Intent(this, BeforeCheckout.class);
                intent.putExtra("Itemname", s);
                startActivity(intent);
                break;

            case R.id.documents:
                s = "Documents";
                intent = new Intent(this, BeforeCheckout.class);
                intent.putExtra("Itemname", s);
                startActivity(intent);
                break;
        }

    }


    private void save() {
        Boolean b = Login.isUserLoggedin();
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = openFileOutput("Login.txt", Context.MODE_PRIVATE);
            if (b == true) {
                fileOutputStream.write(1);
            } else {
                fileOutputStream.write(0);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        save();
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Alert!");
        alertDialog.setCancelable(true);
        alertDialog.setMessage("Do you want to exit App?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                moveTaskToBack(true);
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog alert1 = alertDialog.create();
        alert1.show();
    }
}
