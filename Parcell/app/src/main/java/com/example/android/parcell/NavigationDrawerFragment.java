package com.example.android.parcell;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends android.support.v4.app.Fragment implements mineAdapter.ClickListener {
    public static final String PREF_FILE_NAME = "testpref";//shared preference filemineAdapter
    public static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";
    private RecyclerView recyclerView;
    private boolean mUserLearnedDrawer;
    private boolean mFromSaveInstanceState;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View containerView;
    private mineAdapter mineAdapter;
    public NavigationDrawerFragment() {
        // Required empty public constructor
    }


    public static List<Information> getDataNotLoggedin() {
        List<Information> data = new ArrayList<>();
        int[] icons = {R.mipmap.my_bookings, R.mipmap.email, R.mipmap.call_us, R.mipmap.rate, R.mipmap.shareus, R.mipmap.login};
        String[] titles = {"My Bookings", "Email Us", "Contact Us", "Rate Us", "Share", "Login"};
        for (int i = 0; i < titles.length && i < icons.length; i++) {
            Information current = new Information();
            current.iconId = icons[i];
            current.title = titles[i];
            data.add(current);
        }
        return data;
    }

    public static List<Information> getDataLoggedin() {
        List<Information> data = new ArrayList<>();
        int[] icons = {R.mipmap.my_bookings, R.drawable.address, R.mipmap.email, R.mipmap.call_us, R.mipmap.rate, R.mipmap.shareus, R.mipmap.login};
        String[] titles = {"My Bookings", "My Address", "Email Us", "Contact Us", "Rate Us", "Share", "Logout"};
        for (int i = 0; i < titles.length && i < icons.length; i++) {
            Information current = new Information();
            current.iconId = icons[i];
            current.title = titles[i];
            data.add(current);
        }
        return data;
    }

    public static void saveToPreferences(Context context, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLearnedDrawer = Boolean.valueOf(readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, "false"));
        if (savedInstanceState != null) {
            mFromSaveInstanceState = true;
        }
    }

    private void read() {
        FileInputStream fileInputStream = null;
        int read = -1;
        try {
            fileInputStream = getActivity().openFileInput("Login.txt");
            read = fileInputStream.read();
            if (read == 1) {
                Login.setUserLoggedin(true);
            }
            if (read == 0) {
                Login.setUserLoggedin(false);
            }
        } catch (FileNotFoundException e) {
            Login.setUserLoggedin(false);
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void save() {
        Boolean b = Login.isUserLoggedin();
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = getActivity().openFileOutput("Login.txt", Context.MODE_PRIVATE);
            if (b == true) {
                fileOutputStream.write(1);
            } else {
                fileOutputStream.write(0);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);
        List<Information> list = null;
        read();
        if (Login.isUserLoggedin()) {
            list = getDataLoggedin();
        } else if (!Login.isUserLoggedin()) {
            list = getDataNotLoggedin();
        }
        mineAdapter = new mineAdapter(getActivity(), list);
        recyclerView.setAdapter(mineAdapter);
        mineAdapter.setClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return layout;
    }

    public void setup(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    saveToPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLearnedDrawer + "");
                }
                getActivity().invalidateOptionsMenu();//redraw the menu or action bar again
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();//redraw the menu or action bar again
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (slideOffset < 0.45) {
                    toolbar.setAlpha(1 - slideOffset);
                }
            }
        };
       /* if (!mUserLearnedDrawer && !mFromSaveInstanceState) {
            mDrawerLayout.openDrawer(containerView);
        }*/
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    @Override
    public void itemclick(View view, int position) {
        Intent intent = null;
        if (!Login.isUserLoggedin()) {
            switch (position) {
                case 0:
                    intent = new Intent(getActivity(), My_Bookings.class);
                    break;
                case 1:
                    intent = new Intent(Intent.ACTION_SEND);
                    intent.setData(Uri.parse("mailto:"));
                    String[] to = {"abhishek.thakur@sscbs.du.ac.in"};
                    intent.putExtra(Intent.EXTRA_EMAIL, to);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "[CUSTOMER]Help");
                    intent.setType("message/rfc822");
                    break;
                case 2:
                    intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:9873060360"));
                    break;
                case 3:
                    intent = new Intent(getActivity(), Rate_us.class);
                    break;
                case 4:
                    intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Subject Test");
                    intent.putExtra(Intent.EXTRA_TEXT, "Download app at https://www.google.co.in/");
                    intent.setType("text/plain");
                    break;
                case 5:
                    intent = new Intent(getActivity(), Login.class);
                    intent.putExtra("intent", "two");
                    break;
            }
            startActivity(intent);
        } else if (Login.isUserLoggedin()) {
            switch (position) {
                case 0:
                    intent = new Intent(getActivity(), My_Bookings.class);
                    startActivity(intent);
                    break;
                case 1:
                    intent = new Intent(getActivity(), My_addresses.class);
                    startActivity(intent);
                    break;
                case 2:
                    intent = new Intent(Intent.ACTION_SEND);
                    intent.setData(Uri.parse("mailto:"));
                    String[] to = {"abhishek.thakur@sscbs.du.ac.in"};
                    intent.putExtra(Intent.EXTRA_EMAIL, to);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "[CUSTOMER]Help");
                    intent.setType("message/rfc822");
                    startActivity(intent);
                    break;
                case 3:
                    intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:9873060360"));
                    startActivity(intent);
                    break;
                case 4:
                    intent = new Intent(getActivity(), Rate_us.class);
                    startActivity(intent);
                    break;
                case 5:
                    intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Subject Test");
                    intent.putExtra(Intent.EXTRA_TEXT, "Download app at https://www.google.co.in/");
                    intent.setType("text/plain");
                    startActivity(intent);
                    break;
                case 6:
                    Login.setUserLoggedin(false);
                    save();
                    Toast.makeText(getActivity(), "You have successfully logged out", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), MainActivity.class));
                    break;
            }

        }
    }
}