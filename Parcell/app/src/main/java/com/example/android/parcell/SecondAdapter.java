package com.example.android.parcell;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

public class SecondAdapter extends RecyclerView.Adapter<SecondAdapter.MyViewHolder> {
    List<Searchitem> data = Collections.emptyList();
    private LayoutInflater inflator;
    private Context context;
    private ClickListener clickListener;

    public SecondAdapter(Context context, List<Searchitem> data) {
        inflator = LayoutInflater.from(context);
        this.data = data;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }


    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.singlesearch_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }


    public void onBindViewHolder(MyViewHolder holder, int position) {
        Searchitem current = data.get(position);
        holder.title.setText(current.itemname);
    }


    public int getItemCount() {
        return data.size();
    }

    public interface ClickListener {
        public void itemclick(View view, int position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.listText);
            context = itemView.getContext();
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.itemclick(v, getLayoutPosition());
            }
        }

    }

}
