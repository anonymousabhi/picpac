package com.example.android.parcell;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beardedhen.androidbootstrap.BootstrapButton;
import com.example.android.parcell.beckend.ConnectionDetector;
import com.example.android.parcell.beckend.CustomRequest;
import com.example.android.parcell.beckend.ErrorDialogMessage;
import com.example.android.parcell.beckend.Progress;
import com.example.android.parcell.beckend.VolleySingleton;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Signup extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    CheckBox checkBox;
    Context context;
    com.rengwuxian.materialedittext.MaterialEditText editText_email, editText_password, editText_confirm_password, editText_phone;
    com.beardedhen.androidbootstrap.BootstrapButton button_login, button_singnup;
    android.support.design.widget.TextInputLayout Erroremail, Errorpassword, Errorconfirmpassword, Errorphone;
    RequestQueue queue;
    ConnectionDetector connectionDetector = new ConnectionDetector(this);
    ErrorDialogMessage errorDialogMessage = new ErrorDialogMessage(this);
    Progress progress = new Progress(this);

    Intent intent = null;
    String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        context = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        intent = getIntent();
        s = intent.getStringExtra("intent");
        editText_email = (MaterialEditText) findViewById(R.id.et_email);
        editText_password = (MaterialEditText) findViewById(R.id.et_password);
        editText_confirm_password = (MaterialEditText) findViewById(R.id.et_confirm_password);
        editText_phone = (MaterialEditText) findViewById(R.id.et_phone);
        button_login = (BootstrapButton) findViewById(R.id.button_login);
        button_singnup = (BootstrapButton) findViewById(R.id.button_signup);
        Erroremail = (TextInputLayout) editText_email.getParent();
        Errorpassword = (TextInputLayout) editText_password.getParent();
        Errorconfirmpassword = (TextInputLayout) editText_confirm_password.getParent();
        Errorphone = (TextInputLayout) editText_phone.getParent();
        queue = VolleySingleton.getInstance().getRequestQueue();

        checkBox = (CheckBox) findViewById(R.id.checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    editText_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    editText_confirm_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    editText_password.setInputType(129);
                    editText_confirm_password.setInputType(129);
                }

            }
        });
        button_login.setOnClickListener(this);
        button_singnup.setOnClickListener(this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void generateError(TextInputLayout t, String Message) {
        t.setErrorEnabled(true);
        t.setError(Message);
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void parseData(String phones, String email, String password) {
        HashMap<String, String> params = new HashMap<String, String>();
        long phone = Long.parseLong(phones);
        params.put("phone", phone + "");
        params.put("email", email);
        params.put("password", password);
        String url = "http://dexesnotes.net84.net/register.php";
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String data = response.getString("inserted");
                    if (data != null && data.equals("1")) {
                        errorDialogMessage.show("REGISTRATION SUCCESSFUL");
                    } else {
                        errorDialogMessage.show("ALREADY REGISTERED");
                    }
                    progress.stop();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.stop();
            }
        });
        jsObjRequest.setTag("SetupAccount");
        queue.add(jsObjRequest);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_signup:
                if (connectionDetector.isConnectingToInternet()) {
                    String email, password, confirm_password, phone;
                    phone = editText_phone.getText().toString();
                    email = editText_email.getText().toString();
                    password = editText_password.getText().toString();
                    confirm_password = editText_confirm_password.getText().toString();
                    int count = 0;

                    if (password.length() < 8) {
                        generateError(Errorpassword, "          *Min 8 Characters");
                    } else {
                        count++;
                        Errorpassword.setErrorEnabled(false);
                        if (!confirm_password.equals(password)) {
                            generateError(Errorconfirmpassword, "          *Password mismatch");
                        } else {
                            Errorconfirmpassword.setErrorEnabled(false);
                        }
                    }
                    if (phone.length() != 10) {
                        generateError(Errorphone, "          *Not a valid number");
                    } else {
                        count++;
                        Errorphone.setErrorEnabled(false);
                    }
                    if (!isValidEmail(email)) {
                        generateError(Erroremail, "          *Enter Valid Email");
                    } else {
                        count++;
                        Erroremail.setErrorEnabled(false);
                    }
                    //Toast.makeText(this, count + "", Toast.LENGTH_SHORT).show();
                    if (count == 3) {
                        progress.show();
                        parseData(phone, email, password);
                    } else {
                        Toast.makeText(this, "FILL OUT MISSING DETAILS", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    errorDialogMessage.show();
                }
                break;
            case R.id.button_login:
                intent = new Intent(new Intent(this, Login.class));
                intent.putExtra("intent", s);
                startActivity(intent);
                break;
        }
    }

}
