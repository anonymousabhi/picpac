package com.example.android.parcell;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by abhishek on 1/20/2016.
 */

public class ThirdAdapter extends RecyclerView.Adapter<ThirdAdapter.MyViewHolder> {
    List<Searchitem> data = Collections.emptyList();
    private LayoutInflater inflator;
    private Context context;
    private ClickListener clickListener;

    public ThirdAdapter(Context context, List<Searchitem> data) {
        inflator = LayoutInflater.from(context);
        this.data = data;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }


    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.thirdrow, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }


    public void onBindViewHolder(MyViewHolder holder, int position) {
        Searchitem current = data.get(position);
        holder.title.setText(current.itemname);
        holder.numbers.setText(current.count + "");
    }


    public int getItemCount() {
        return data.size();
    }

    public interface ClickListener {
        public void itemclick(View view, int position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        TextView numbers;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.listText);
            numbers = (TextView) itemView.findViewById(R.id.number);
            context = itemView.getContext();
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.itemclick(v, getLayoutPosition());
            }
        }
    }

}
