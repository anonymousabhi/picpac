package com.example.android.parcell.beckend;

/**
 * Created by abhishek on 1/11/2016.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class ErrorDialogMessage {


    Context context;


    public ErrorDialogMessage(Context c) {

        this.context = c;
    }


    public void show() {


        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage("CHECK INTERNET CONNECTION");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void show(String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(message);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}