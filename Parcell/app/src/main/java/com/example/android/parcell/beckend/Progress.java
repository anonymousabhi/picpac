package com.example.android.parcell.beckend;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by baymax on 23/12/15.
 */
public class Progress {


    ProgressDialog Dialog;
    Context context;
    boolean ifstarted;

    public Progress(Context c) {

        ifstarted = false;
        context = c;
    }

    public void show() {

        if (!ifstarted) {
            Dialog = new ProgressDialog(context);
            Dialog.setTitle("Processing");
            Dialog.setMessage("Please Wait.....");
            Dialog.setCancelable(true);
            Dialog.show();
            ifstarted = true;
        }

    }

    public void stop() {

        if (ifstarted) {

            Dialog.cancel();
        }


        ifstarted = false;

    }
}