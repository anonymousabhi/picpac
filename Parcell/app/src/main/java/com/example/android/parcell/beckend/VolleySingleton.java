package com.example.android.parcell.beckend;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by abhishek on 1/11/2016.
 */
public class VolleySingleton {


    public static VolleySingleton sInstance = null;

    private RequestQueue mRequestQueue;

    private VolleySingleton(){

        mRequestQueue = Volley.newRequestQueue(MyApplication.getAppContext());
    }

    public static VolleySingleton getInstance(){

        if(sInstance == null){

            sInstance = new VolleySingleton();
        }
        return sInstance;
    }

    public RequestQueue getRequestQueue(){

        return mRequestQueue;
    }
}

