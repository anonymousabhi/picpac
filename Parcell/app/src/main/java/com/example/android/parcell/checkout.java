package com.example.android.parcell;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.AwesomeTextView;

public class checkout extends AppCompatActivity {
    Toolbar toolbar;
    RadioButton selected_radioButton;
    Spinner weight_spinner;
    com.beardedhen.androidbootstrap.AwesomeTextView price_TextView;
    String from, to;
    TextView weight;
    Button buttonproceed;
    ImageView right;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        from = intent.getStringExtra("from");
        to = intent.getStringExtra("to");
        price_TextView = (AwesomeTextView) findViewById(R.id.price);
        weight_spinner = (Spinner) findViewById(R.id.weight_spinner);
        ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(this, R.array.weight, R.layout.support_simple_spinner_dropdown_item);
        weight_spinner.setAdapter(arrayAdapter);
        buttonproceed = (Button) findViewById(R.id.proceed);
        right = (ImageView) findViewById(R.id.right);
        buttonproceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Login.isUserLoggedin() == false) {
                    Intent intent;
                    intent = new Intent(checkout.this, Login.class);
                    intent.putExtra("intent", "one");
                    startActivity(intent);
                } else {
                    startActivity(new Intent(checkout.this, GetAddress.class));
                }
            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Login.isUserLoggedin() == false) {
                    startActivity(new Intent(checkout.this, Login.class));
                } else {
                    startActivity(new Intent(checkout.this, GetAddress.class));
                }
            }
        });
        weight_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                weight = (TextView) view;
                if (from.equals("Delhi")) {
                    if (to.equals("Delhi")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  50");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  100");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  350");
                        }
                    }
                    if (to.equals("Noida")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  100");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  400");
                        }

                    }
                    if (to.equals("Gurgaon")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  100");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  400");
                        }
                    }
                    if (to.equals("North India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  120");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  170");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  220");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  270");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  320");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  370");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  420");
                        }
                    }
                    if (to.equals("East India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("North East India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("West India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("South India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("Central India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }

                }
                if (from.equals("Noida")) {
                    if (to.equals("Noida")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  50");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  100");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  350");
                        }
                    }
                    if (to.equals("Uttar Pradesh")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  100");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  400");
                        }

                    }
                    if (to.equals("Delhi")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  100");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  400");
                        }

                    }
                    if (to.equals("Gurgaon")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  120");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  170");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  220");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  270");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  320");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  370");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  420");
                        }
                    }
                    if (to.equals("North India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("East India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("North East India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("West India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("South India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("Central India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }

                }
                if (from.equals("Gurgaon")) {
                    if (to.equals("Gurgaon")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  50");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  100");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  350");
                        }
                    }
                    if (to.equals("Haryana")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  100");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  400");
                        }

                    }
                    if (to.equals("Delhi")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  100");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  400");
                        }

                    }
                    if (to.equals("Noida")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  120");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  170");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  220");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  270");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  320");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  370");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  420");
                        }
                    }
                    if (to.equals("North India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("East India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("North East India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("West India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("South India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }
                    if (to.equals("Central India")) {
                        if (weight.getText().equals("Upto 0.5Kg")) {
                            price_TextView.setText("Rs  150");
                        } else if (weight.getText().equals("0.5Kg-1Kg")) {
                            price_TextView.setText("Rs  200");
                        } else if (weight.getText().equals("1Kg-2Kg")) {
                            price_TextView.setText("Rs  250");
                        } else if (weight.getText().equals("2Kg-5Kg")) {
                            price_TextView.setText("Rs  300");
                        } else if (weight.getText().equals("5Kg-10Kg")) {
                            price_TextView.setText("Rs  350");
                        } else if (weight.getText().equals("10Kg-20Kg")) {
                            price_TextView.setText("Rs  400");
                        } else if (weight.getText().equals("20Kg-25Kg")) {
                            price_TextView.setText("Rs  450");
                        }
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, BeforeCheckout.class));
    }
}

